package apk.team.e_blk.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import apk.team.e_blk.R;
import apk.team.e_blk.adapter.ListAdapterPelatihan;
import apk.team.e_blk.model.JurusanPelatihan;

public class Pelatihan extends AppCompatActivity  implements SearchView.OnQueryTextListener {

    ListView listview;
    ActionMode actionmode;
    ActionMode.Callback amCallback;
    ProgressDialog progressdialog;
    ServerRequest serverrequest;
    List<JurusanPelatihan> list;
    ListAdapterPelatihan adapter;
    JurusanPelatihan selectedList;
    SwipeRefreshLayout swipeRefresh;
    public static final String KEY_KATEGORI = "id_jurusan";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelatihan);
        getSupportActionBar().setTitle("Pelatihan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        serverrequest = new ServerRequest();
        listview = (ListView) findViewById(R.id.listView1);

        list = new ArrayList<JurusanPelatihan>();
        new MainActivityAsync().execute("load");

        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.refresh);
        swipeRefresh.setColorSchemeResources(
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_light,
                android.R.color.holo_blue_bright
        );
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refreshitem();
                    }
                    void refreshitem() {
                        new MainActivityAsync().execute("load");
                        onItemLoad();
                    }
                    void onItemLoad(){
                        swipeRefresh.setRefreshing(false);
                    }
                }, 5000);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_latihan_menu, menu);
        SearchManager searchmanager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchview = (SearchView) menu.findItem(R.id.action_menu_search).getActionView();
        searchview.setSearchableInfo(searchmanager.getSearchableInfo(getComponentName()));
        searchview.setIconifiedByDefault(false);
        searchview.setOnQueryTextListener(this);
        searchview.setQueryHint("Cari Jurusan Pelatihan");

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private List<JurusanPelatihan> processResponse(String response) {
        List<JurusanPelatihan> list = new ArrayList<JurusanPelatihan>();
        try {
            JSONObject jsonObj = new JSONObject(response);
            JSONArray jsonArray = jsonObj.getJSONArray("jurusan_pelatihan");
            JurusanPelatihan jur = null;
            for (int i=0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                jur = new JurusanPelatihan();
                jur.setId_jurusan(obj.getInt("ID_JURUSAN"));
                jur.setNama_jurusan(obj.getString("NAMA_JURUSAN"));
                jur.setDeskripsi(obj.getString("DESKRIPSI"));
                jur.setGambar(obj.getString("IMG"));
                list.add(jur);
            }

        } catch (JSONException e) {
            e.getMessage();
        }
        return list;
    }

    private void createListview() {
        final Activity that = this;
        adapter = new ListAdapterPelatihan(getApplicationContext(), list);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedList = (JurusanPelatihan) adapter.getItem(position);
                //panggil class detailMateri dengan mengirimkan nama,img dan deskripsi
                Intent in = new Intent(that, DetailMateri.class);
                in.putExtra(KEY_KATEGORI, selectedList.getId_jurusan());
                in.putExtra("NAMA_JURUSAN", selectedList.getNama_jurusan());
                in.putExtra("IMG", selectedList.getGambar());
                in.putExtra("DESKRIPSI", selectedList.getDeskripsi());
                that.startActivity(in);
            }
        });
    }


    private class MainActivityAsync extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            progressdialog = new ProgressDialog(Pelatihan.this);
            progressdialog.setMessage("Sedang Proses...");
            progressdialog.setIndeterminate(false);
            progressdialog.setCancelable(false);
            progressdialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response = serverrequest.sendGetRequest(ServerRequest.urlSelectAllJurusan);
            list = processResponse(response);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
           progressdialog.dismiss();
           runOnUiThread(new Runnable() {
               @Override
               public void run() {
                   createListview();
               }
           });
        }

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return true;
    }

}
