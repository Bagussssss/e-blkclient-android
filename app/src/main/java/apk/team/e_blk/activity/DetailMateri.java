package apk.team.e_blk.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import apk.team.e_blk.R;
import apk.team.e_blk.adapter.ListAdapterMateri;
import apk.team.e_blk.model.MateriPelatihan;
import static apk.team.e_blk.activity.ServerRequest.loadImginServer;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class DetailMateri extends AppCompatActivity{
    ListView listview;
    ActionMode actionmode;
    ActionMode.Callback amCallback;
    ProgressDialog progressdialog;
    ServerRequest serverrequest;
    List<MateriPelatihan> list;
    ListAdapterMateri adapter;
    MateriPelatihan selectedList;
    private int idJurusan;
    String namaPelatihan;
    TextView tampilNamaPelatihan, TampilDeskripsi;
    String imgView, deskripsiView;
    ImageView tampilImgViewBeranda;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailmateri_activity);
        namaPelatihan = getIntent().getStringExtra("NAMA_JURUSAN");
//        getSupportActionBar().setTitle("Materi Pelatihan");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().hide();

        serverrequest = new ServerRequest();
        listview = (ListView) findViewById(R.id.listViewMateri);
        idJurusan = getIntent().getIntExtra(Pelatihan.KEY_KATEGORI, -1);

        tampilNamaPelatihan = (TextView) findViewById(R.id.NamaPelatihan);
        tampilNamaPelatihan.setText(namaPelatihan);

        deskripsiView = getIntent().getStringExtra("DESK");
        TampilDeskripsi = (TextView) findViewById(R.id.DeskripsiPelatihan);
        TampilDeskripsi.setText(deskripsiView);

        //load image in server
        tampilImgViewBeranda = (ImageView) findViewById(R.id.imgPelatihanBeranda);
        imgView = getIntent().getStringExtra("IMG");
        Glide.with(this)
                .load(loadImginServer + imgView)
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .placeholder(R.drawable.ic_launcher_foreground)
                .centerCrop()
                .into(tampilImgViewBeranda);

        list = new ArrayList<MateriPelatihan>();
        new MateriAsync().execute("load");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private List<MateriPelatihan> processResponse(String response) {
        List<MateriPelatihan> list = new ArrayList<MateriPelatihan>();
        try {
            JSONObject jsonObj = new JSONObject(response);
            JSONArray jsonArray = jsonObj.getJSONArray("materi");
            MateriPelatihan mp = null;
            for (int i=0; i<jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                mp = new MateriPelatihan();
                mp.setId_pelatihan(obj.getInt("ID_MATERI"));
                mp.setMateri_pelatihan(obj.getString("NAMA_MATERI"));
                mp.setId_jurusan(obj.getInt("ID_JURUSAN"));
                mp.setIsi_materi(obj.getString("ISI_MATERI"));
                mp.setTahap(obj.getString("TAHAP"));
                list.add(mp);
            }
        } catch (JSONException e) {
            e.getMessage();
        }
        return list;
    }

    private void populateListView() {
        adapter = new ListAdapterMateri(getApplicationContext(), list);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedList = (MateriPelatihan) adapter.getItem(position);
                Intent in = new Intent(getApplicationContext(), isiMateri.class);
                in.putExtra("id_materi", selectedList.getId_jurusan());
                in.putExtra("materi_pelatihan", selectedList.getMateri_pelatihan());
                in.putExtra("isi_materi", selectedList.getIsi_materi());
                in.putExtra("tahap", selectedList.getTahap());
                startActivity(in);
            }
        });
    }


    private class MateriAsync extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            progressdialog = new ProgressDialog(DetailMateri.this);
            progressdialog.setMessage("Sedang Mengambil Data...");
            progressdialog.setIndeterminate(false);
            progressdialog.setCancelable(false);
            progressdialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String response = serverrequest.sendGetRequest(ServerRequest.getUrlSelectAllMateri + "?id_jurusan=" + idJurusan);
            list = processResponse(response);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressdialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    populateListView();
                }
            });
        }
    }
}
