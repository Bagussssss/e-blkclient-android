package apk.team.e_blk.activity;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;

import apk.team.e_blk.R;

public class isiMateri extends AppCompatActivity {
    PDFView pdfview;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi);
        String namePDF = getIntent().getStringExtra("isi_materi");
        getSupportActionBar().setTitle("ini" + namePDF);

        pdfview.fromFile(new File("https://cpns.kemenkumham.go.id/assets/upload/dokumenunggah/pengumuman-seleksi-administrasi.pdf"))
                .enableSwipe(true)
                .load();


    }


}
