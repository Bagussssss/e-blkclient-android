package apk.team.e_blk.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import apk.team.e_blk.R;

public class TentangKami extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tentangkami);
        getSupportActionBar().setTitle("Tentang Kami");
    }
}
