package apk.team.e_blk.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import apk.team.e_blk.R;

public class MainActivity extends AppCompatActivity {

    private CardView btnPelatihan, btnTatacara, btnbantuan, btntentangkami;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPelatihan = (CardView) findViewById(R.id.btnPelatihan);

        btnPelatihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inPelatihan = new Intent(getApplicationContext(), Pelatihan.class);
                startActivity(inPelatihan);
            }
        });
        btnTatacara = (CardView) findViewById(R.id.btnTatacara);
        btnTatacara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inTatacara = new Intent(getApplicationContext(), TataCara.class);
                startActivity(inTatacara);
            }
        });

        btnbantuan = (CardView) findViewById(R.id.btnBantuan);
        btnbantuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), DetailMateri.class);
                startActivity(i);
            }
        });

        btntentangkami = (CardView) findViewById(R.id.btnTentangkami);
        btntentangkami.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), TentangKami.class);
                startActivity(i);
            }
        });


    }
}
