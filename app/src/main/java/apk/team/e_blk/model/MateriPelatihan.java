package apk.team.e_blk.model;

public class MateriPelatihan {
    private Integer id_pelatihan;
    private String materi_pelatihan;
    private Integer id_jurusan;
    private String isi_materi;
    private String tahap;

    public Integer getId_pelatihan() {
        return id_pelatihan;
    }

    public void setId_pelatihan(Integer id_pelatihan) {
        this.id_pelatihan = id_pelatihan;
    }

    public String getMateri_pelatihan() {
        return materi_pelatihan;
    }

    public void setMateri_pelatihan(String materi_pelatihan) {
        this.materi_pelatihan = materi_pelatihan;
    }

    public Integer getId_jurusan() {
        return id_jurusan;
    }

    public void setId_jurusan(Integer id_jurusan) {
        this.id_jurusan = id_jurusan;
    }

    public String getIsi_materi() {
        return isi_materi;
    }

    public void setIsi_materi(String isi_materi) {
        this.isi_materi = isi_materi;
    }

    public String getTahap() {
        return tahap;
    }

    public void setTahap(String tahap) {
        this.tahap = tahap;
    }
}
