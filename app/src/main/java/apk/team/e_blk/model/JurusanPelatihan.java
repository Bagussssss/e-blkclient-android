package apk.team.e_blk.model;

public class JurusanPelatihan {

    private Integer id_jurusan;
    private String nama_jurusan;
    private String deskripsi;
    private String gambar;


    public Integer getId_jurusan() {
        return id_jurusan;
    }

    public void setId_jurusan(Integer id_jurusan) {
        this.id_jurusan = id_jurusan;
    }

    public String getNama_jurusan() {
        return nama_jurusan;
    }

    public void setNama_jurusan(String nama_jurusan) {
        this.nama_jurusan = nama_jurusan;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
