package apk.team.e_blk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import apk.team.e_blk.R;
import apk.team.e_blk.model.MateriPelatihan;

public class ListAdapterMateri extends BaseAdapter implements Filterable {

    Context context;
    List<MateriPelatihan> list, filterd;

    public ListAdapterMateri(Context context, List<MateriPelatihan> list) {
        this.context = context;
        this.list = list;
        this.filterd = this.list;
    }

    @Override
    public int getCount() {
        return filterd.size();
    }

    @Override
    public Object getItem(int position) {
        return filterd.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(this.context);
            convertView = inflater.inflate(R.layout.konten_list_materi, null);
        }
        MateriPelatihan mp = filterd.get(position);

        TextView tampilMateri = (TextView) convertView.findViewById(R.id.tvMateriPelatihan);
        tampilMateri.setText(mp.getMateri_pelatihan());

        TextView tampilTahap = (TextView) convertView.findViewById(R.id.tvTahapMateri);
        tampilTahap.setText(mp.getTahap());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        MateriFilter filter = new MateriFilter();
        return filter;
    }

    private class MateriFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<MateriPelatihan> filteredData = new ArrayList<MateriPelatihan>();
            FilterResults result = new FilterResults();
            String filterString = constraint.toString().toLowerCase();
            for (MateriPelatihan mp: list) {
                if (mp.getMateri_pelatihan().toString().toLowerCase().contains(filterString)) {
                    filteredData.add(mp);
                }
            }
            result.count = filteredData.size();
            result.values = filteredData;
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filterd = (List<MateriPelatihan>) results.values;
            notifyDataSetChanged();
        }
    }
}
