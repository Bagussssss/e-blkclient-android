package apk.team.e_blk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import apk.team.e_blk.R;
import apk.team.e_blk.activity.ServerRequest;
import apk.team.e_blk.model.JurusanPelatihan;
import static apk.team.e_blk.activity.ServerRequest.loadImginServer;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
public class ListAdapterPelatihan extends BaseAdapter implements Filterable {

    Context context;
    List<JurusanPelatihan> list;
    List<JurusanPelatihan> filterd;
    ServerRequest serverrequest;


    public ListAdapterPelatihan(Context context, List<JurusanPelatihan> list) {
        this.context = context;
        this.list = list;
        this.filterd = this.list;
    }



    @Override
    public int getCount() {
        return filterd.size();
    }

    @Override
    public Object getItem(int position) {
        return filterd.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(this.context);
            convertView = inflater.inflate(R.layout.konten_list_pelatihan, null);
        }
        JurusanPelatihan jurusan = filterd.get(position);

        TextView tampil_namajurusan = (TextView) convertView.findViewById(R.id.tvNamaPelatihan);
        tampil_namajurusan.setText(jurusan.getNama_jurusan());

        TextView tampil_deskripsi = (TextView) convertView.findViewById(R.id.tvDeskripsiPelatihan);
        tampil_deskripsi.setText(jurusan.getDeskripsi());

        //load gambar
        ImageView gambar = (ImageView) convertView.findViewById(R.id.imgPelatihan);
        Glide.with(context)
                .load(loadImginServer  + jurusan.getGambar())
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .placeholder(R.drawable.ic_launcher_foreground)
                .centerCrop()
                .circleCrop()
                .into(gambar);


        return convertView;
    }

    @Override
    public Filter getFilter() {
        PelatihanFilter filter = new PelatihanFilter();
        return filter;
    }

    private class PelatihanFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<JurusanPelatihan> filteredData = new ArrayList<JurusanPelatihan>();
            FilterResults result = new FilterResults();
            String filterString = constraint.toString().toLowerCase();
            for (JurusanPelatihan jur: list) {
                if (jur.getNama_jurusan().toLowerCase().contains(filterString)) {
                    filteredData.add(jur);
                }
            }
            result.count = filteredData.size();
            result.values = filteredData;
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filterd = (List<JurusanPelatihan>) results.values;
            notifyDataSetChanged();
        }

    }

}
